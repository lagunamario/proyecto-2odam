package com.mlaguna;

import javax.swing.*;

public class Vista extends JFrame {
    private JPanel panel1;

    JButton btnNuevaPuntuacion;
    JButton btnModificarPuntuacion;
    JButton btnEliminarPuntuacion;
    JButton btnActualizar;
    JList listPuntuacion;
    JTextField txtNombre;
    JTextField txtPuntuacion;

    DefaultListModel<Puntuacion> dlmPuntuacion;

    public Vista() {
        this.setTitle("Administrador de puntuaciones");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);

        inicializar();
    }

    private void inicializar() {
        dlmPuntuacion = new DefaultListModel<>();
        listPuntuacion.setModel(dlmPuntuacion);
    }
}
