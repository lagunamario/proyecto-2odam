package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class SerpienteMuerte extends BaseActor{

	String[] imagenesMuerte = {
			"SerpienteDeath01.png",
			"SerpienteDeath02.png",
			"SerpienteDeath03.png"
	};
	
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public SerpienteMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesMuerte, 0.1f, false);
		addAction( Actions.fadeOut(1f) );
        addAction( Actions.after( Actions.removeActor() ) );
		setScale(2);
		
	}
	
	

}
