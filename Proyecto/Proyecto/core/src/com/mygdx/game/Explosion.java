package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Explosion extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Explosion(float x, float y, Stage s) {
		super(x, y, s);
		this.loadAnimationFromSheet("explosion-6.png", 1, 8, 0.08f, false);
		setSize(200, 200);
        addAction( Actions.fadeOut(1f) );
        addAction( Actions.after( Actions.removeActor() ) );
	}

}
