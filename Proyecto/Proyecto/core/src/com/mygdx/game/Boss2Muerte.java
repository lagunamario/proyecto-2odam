package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Boss2Muerte extends BaseActor{

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Boss2Muerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromSheet("Boss2Death.png", 1, 37, 0.08f, false );
		setScale(2);
		
	}
	
	

}
