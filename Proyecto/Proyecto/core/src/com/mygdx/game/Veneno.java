package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Veneno extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Veneno(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("veneno.png");
		setSize(200, 200);
        addAction( Actions.fadeOut(1f) );
        addAction( Actions.after( Actions.removeActor() ) );
	}

}
