package com.mygdx.game;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor{
	private int puerto;
	private String mensaje;
	Personaje personaje;
	
	private Servidor(int puerto) {
		this.puerto = puerto;
		arrancarServidor();
	}

	private void arrancarServidor() {
		
		try {
			ServerSocket socketServidor = new ServerSocket(puerto);
			Socket clienteConectado = socketServidor.accept();
			PrintStream salida = new PrintStream(clienteConectado.getOutputStream());
			String recibido;
			DataInputStream entrada = new DataInputStream(clienteConectado.getInputStream());
			
			while(true) {
				recibido = entrada.readLine();
				
				if (recibido.equalsIgnoreCase("Nombre")) {
					salida.println(personaje.getNombre());
				}
				else if (recibido.equalsIgnoreCase("Puntuacion")) {
					salida.println(personaje.getPuntuacion());
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new Servidor(9999);

	}

}
