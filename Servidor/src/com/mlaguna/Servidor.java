package com.mlaguna;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class Servidor {
    private ServerSocket socketServidor;
    private int puerto;
    Vista vista;
    Modelo modelo;
    Controlador controlador;

    private Codec codec;
    private String mensajeDescifrado;
    private String mensajeCifrado;
    private String mensajeDescifrado2;
    private String listadoCifrado;

    public Servidor(int puerto, Vista vista, Modelo modelo, Controlador controlador) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException {
                this.puerto = puerto;
                codec = new Codec();
                this.vista = vista;
                this.modelo = modelo;
                this.controlador = controlador;
                arrancarServidor();
            }

            private void arrancarServidor() throws IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
                socketServidor = new ServerSocket(this.puerto);
                Socket clienteConectado = socketServidor.accept();
                PrintStream salida = new PrintStream(clienteConectado.getOutputStream());
                String recibido;
                DataInputStream entrada = new DataInputStream(clienteConectado.getInputStream());

                while (true){
                    recibido = entrada.readLine();
                    System.out.println("Mensaje Recibido cifrado: " + recibido);
                    mensajeDescifrado = codec.descrifrar(recibido);
                    System.out.println("Mensaje descifrado: " + mensajeDescifrado);
                    vista.txtNombre.setText(mensajeDescifrado);

                    String mensajePuntuacion = "Peticion de puntuacion de " + mensajeDescifrado;
                    mensajeCifrado = codec.cifrar(mensajePuntuacion);
                    System.out.println("Mensaje cifrado para el cliente: " + mensajeCifrado);
                    salida.println(mensajeCifrado);

                    recibido = entrada.readLine();
                    System.out.println("Mensaje recibido cifrado: " + recibido);
                    mensajeDescifrado2 = codec.descrifrar(recibido);
                    System.out.println("Mensaje descifrado: " + mensajeDescifrado2);
                    vista.txtPuntuacion.setText(mensajeDescifrado2);
                    Puntuacion p = new Puntuacion();
                    controlador.setDatos(p);
                    modelo.guardarPuntuacion(p);
                    controlador.listar(modelo.getPuntuaciones());
                    controlador.limpiarCampos();

                    System.out.println("Listado de puntuaciones para el cliente: ");
                    System.out.println(modelo.getPuntuaciones());
                    listadoCifrado =  codec.cifrar(String.valueOf(modelo.getPuntuaciones()));
                    System.out.println("Listado cifrado: " + listadoCifrado);
                    salida.println(listadoCifrado);


        }


    }
}
