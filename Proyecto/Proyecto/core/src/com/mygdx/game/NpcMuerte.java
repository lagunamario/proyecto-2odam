package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class NpcMuerte extends BaseActor{

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public NpcMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromSheet("npcMuerte.png", 1, 24, 0.08f, false);
		setScale(2);
		
		
	}
	
	

}
