package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Suelo extends Solid {

	public Suelo(float x, float y, Stage s) {
		super(x, y, 32, 32, s);
		loadTexture("suelo.png");
        setBoundaryRectangle();
	}

	
}
