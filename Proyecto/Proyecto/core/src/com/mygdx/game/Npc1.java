package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Npc1 extends BaseActor {
	String[] imagenes = {
			"npc1-1.png",
			"npc1-2.png",
			"npc1-3.png",
			"npc1-4.png",
			"npc1-5.png",
			"npc1-6.png",
			"npc1-7.png",
			"npc1-8.png",
	};

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Npc1(float x, float y, Stage s) {
		super(x, y, s);
	
		//loadTexture("npc1.png");
		//setSpeed(400);
		this.loadAnimationFromFiles(imagenes, 0.1f, true);
		setSize(64, 96);
		setVisible(true);
		setY(100);
		setBoundaryRectangle();
		
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
		super.act(dt);
		applyPhysics(dt);
	} 

}
