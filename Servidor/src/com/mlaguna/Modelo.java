package com.mlaguna;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {
    private final static String COLECCION_PUNTUACION = "Puntuaciones";
    private final static String DATABASE = "Puntuaciones";

    private MongoClient cliente;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionPuntuaciones;

    public void conectar(){
        cliente = new MongoClient();
        baseDatos = cliente.getDatabase(DATABASE);
        coleccionPuntuaciones = baseDatos.getCollection(COLECCION_PUNTUACION);

    }

    public void desconectar(){
        cliente.close();
    }

    public void guardarPuntuacion(Puntuacion puntuacion){
        coleccionPuntuaciones.insertOne(puntuacionToDocument(puntuacion));
    }

    public void modificarPuntuacion(Puntuacion p){
        coleccionPuntuaciones.replaceOne(new Document("_id", p.getId()),
                puntuacionToDocument(p));
    }

    public void borrarPuntuacion(Puntuacion p){
        coleccionPuntuaciones.deleteOne(puntuacionToDocument(p));
    }
    public List<Puntuacion> getPuntuaciones(){
        ArrayList<Puntuacion> lista = new ArrayList<>();
        Iterator<Document> it = coleccionPuntuaciones.find().iterator();
        while(it.hasNext()){
            lista.add(documentToPuntuacion(it.next()));
        }
        return lista;
    }

    private Puntuacion documentToPuntuacion(Document document) {
        Puntuacion p = new Puntuacion();
        p.setId(document.getObjectId("_id"));
        p.setNombre(document.getString("nombre"));
        p.setPuntuacion(document.getInteger("puntuacion"));
        return p;
    }

    private Document puntuacionToDocument(Puntuacion puntuacion) {
        Document documento = new Document();
        documento.append("nombre", puntuacion.getNombre());
        documento.append("puntuacion", puntuacion.getPuntuacion());
        return documento;
    }

}
