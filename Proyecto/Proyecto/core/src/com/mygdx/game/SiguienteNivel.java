package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class SiguienteNivel extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public SiguienteNivel(float x, float y, Stage s) {
		super(x, y, s);
		
		setSize(32, 32);
		setVisible(true);
		setBoundaryRectangle();
		
		
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
		super.act(dt);
		applyPhysics(dt);
	} 

}
