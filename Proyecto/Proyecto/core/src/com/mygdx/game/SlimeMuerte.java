package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class SlimeMuerte extends BaseActor{

	String[] imagenesMuerte = {
			"slime-die-0.png",
			"slime-die-1.png",
			"slime-die-2.png",
			"slime-die-3.png",
	};
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public SlimeMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesMuerte, 0.08f, false);
		setScale(2);
	
	}
	
	

}
