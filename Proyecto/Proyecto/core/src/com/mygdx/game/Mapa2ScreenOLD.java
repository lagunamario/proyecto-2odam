package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.ArrayList;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;


public class Mapa2ScreenOLD extends BaseScreen {
Personaje personaje;
	
	int vida;
	Label vidaLabel;
	Label mensajeLabel;
	private BaseActor vidaImagen1;
	private BaseActor vidaImagen2;
	private BaseActor vidaImagen3;
	Parametros par;

	@Override
	public void initialize() {
		TilemapActor tma = new TilemapActor("mapa3.tmx", mainStage);
		
		for (MapObject obj : tma.getRectangleList("Solid") ){
            MapProperties props = obj.getProperties();
            new Solid( (float)props.get("x"), (float)props.get("y"),
                (float)props.get("width"), (float)props.get("height"), 
                mainStage );
            System.out.println("Hola");
        }
		
		
		
		MapObject startPoint = tma.getRectangleList("start").get(0);
        MapProperties startProps = startPoint.getProperties();
        personaje = new Personaje( (float)startProps.get("x"), (float)startProps.get("y"), mainStage);
        
        /*for (MapObject obj : tma.getTileList("Suelo") )
        {
            MapProperties props = obj.getProperties();
            new Suelo( (float)props.get("x"), (float)props.get("y"), mainStage );
        }*/
        
   
        	vidaLabel = new Label("x " + vida, BaseGame.labelStyle);
        	vidaLabel.setColor(Color.PINK);
        	mensajeLabel = new Label("...", BaseGame.labelStyle);
        	mensajeLabel.setVisible(false);
        	
        	vidaImagen1 = new BaseActor(0, 0, uiStage);
        	vidaImagen1.loadTexture("heart-icon.png");
        	vidaImagen2 = new BaseActor(0, 0, uiStage);
        	vidaImagen2.loadTexture("heart-icon.png");
        	vidaImagen3 = new BaseActor(0, 0, uiStage);
        	vidaImagen3.loadTexture("heart-icon.png");
        	
        	uiTable.pad(20);
        	uiTable.add(vidaImagen1);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen2);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen3);
        	//uiTable.add().expandX();
        	uiTable.row();
        	uiTable.add(mensajeLabel).colspan(8).expandX().expandY();
        	//uiTable.row();
        	
        	
        
        	personaje.toFront();
    	}

	@Override
	public void update(float dt) {
		
		 for (BaseActor actor : BaseActor.getList(mainStage, Solid.class.getName())){
			 Solid solid = (Solid)actor;
			 
			 
			 if (personaje.overlaps(solid) && solid.isEnabled()){
				 //System.out.println("ha");
				 Vector2 offset = personaje.preventOverlap(solid);
	                if (offset != null){
	                    if ( Math.abs(offset.x) > Math.abs(offset.y) ) {
	                        personaje.velocityVec.x = 0;
	                    }
	                    else {
	                        personaje.velocityVec.y = 0;
	                    }
	                }
	            }
	        }
	}
}
