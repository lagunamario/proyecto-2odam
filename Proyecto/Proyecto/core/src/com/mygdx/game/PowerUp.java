package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class PowerUp extends BaseActor {
	String[] proyectil = {
			"powerup1.png",
			"powerup2.png",
			"powerup3.png",
			"powerup4.png",
			"powerup5.png",
			"powerup6.png",
			"powerup7.png",
			"powerup8.png",
			"powerup9.png",
			"powerup10.png",
			"powerup11.png",
			"powerup12.png"
			};
	Personaje personaje;
	
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa el valor de X
	 * @param y = representa el valor de Y
 	 * @param s = representa por que Stage va a ser influenciado
	 * @param personaje = se le pasa un objeto de la clase Personaje para poder coger un atributo de ella
	 */
	public PowerUp(float x, float y, Stage s, Personaje personaje) {
		super(x, y, s);
		this.personaje = personaje;
		loadAnimationFromFiles(proyectil, 0.08f, false);
		setSpeed(400);
		setSize(25, 25);
		addAction( Actions.fadeOut(1f) );
        addAction( Actions.after( Actions.removeActor() ) );
	
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
		super.act(dt);
		applyPhysics(dt);
		
		if(personaje.getScaleX() == 1) {
			//System.out.println("Miras a la derecha " + personaje.derecha);
			//this.setAcceleration(getX() + 10);
			this.accelerationVec.x = 10;
		}
		else{
			//System.out.println("Miras a la izquierda " + personaje.derecha );
			//this.setAcceleration(getX() - 10);
			setScaleX(-1);
			velocityVec.x = -400;
		}
		
	}
	
}
