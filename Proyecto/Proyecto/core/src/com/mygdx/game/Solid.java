package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Solid extends BaseActor{
	private boolean enabled;
	
	/**
	 * Constructor de la clase Solid que extiende de BaseActor. Se usa principalmente para zonas donde
	 * ningun elemento puede pasar (suelos, paredes etc)
	 * @param x = representa el valor de X
	 * @param y = representa el valor de Y
	 * @param width = representa la anchura
	 * @param height = representa la altura
	 * @param s = representa el stage por el cual va a ser influenciado
	 */
    public Solid(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		setSize(width, height);
        //setBoundaryPolygon(8);
		setBoundaryRectangle();
        enabled = true;
        System.out.println(x + " " + y + " " + width + " " + height);
	}

    public void setEnabled(boolean b){
        enabled = b;
    }

    public boolean isEnabled(){
        return enabled;
    }

}
