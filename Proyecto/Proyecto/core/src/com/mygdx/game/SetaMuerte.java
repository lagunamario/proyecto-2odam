package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class SetaMuerte extends BaseActor{

	String[] imagenesMuerte = {
			"DeathEnemy201.png",
			"DeathEnemy202.png",
			"DeathEnemy203.png",
			"DeathEnemy204.png",
	};
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public SetaMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesMuerte, 0.08f, false);
		setScale(2);
	
	}
	
	

}
