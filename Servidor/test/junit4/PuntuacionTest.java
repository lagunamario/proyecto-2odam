package junit4;

import org.junit.Test;

import static org.junit.Assert.*;

public class PuntuacionTest {
    Puntuacion test;

    @Test
    public void testGetNombre(){
        test= new Puntuacion();
        test.setNombre("Mario");
        String actual = test.getNombre();
        String esperado = "Mario";
        assertEquals(esperado, actual);
    }

    @Test
    public void testGetPuntuacion(){
    test = new Puntuacion();
    test.setPuntuacion(100);
    int actual = test.getPuntuacion();
    int esperado = 100;
    assertEquals(esperado, actual);
    }

    @Test
    public void testSetNombre(){
        test = new Puntuacion();
        test.setNombre("Laguna");
        String actual = "Laguna";
        String esperado = test.getNombre();
        assertEquals(esperado, actual);
    }

    @Test
    public void testSetPuntuacion(){
        test = new Puntuacion();
        test.setPuntuacion(100);
        int actual = 100;
        int esperado = test.getPuntuacion();
        assertEquals(esperado, actual);
    }

    @Test
    public void testGetNombreNegativo(){
        test = new Puntuacion();
        test.setNombre("Mario");
        String actual = test.getNombre();
        String esperado = "Mario";
        assertNotEquals("QWERTY", actual);
    }

    @Test
    public void testGetPuntuacionNegativo(){
        test = new Puntuacion();
        test.setPuntuacion(100);
        int actual = test.getPuntuacion();
        int esperado = 100;
        assertNotEquals(2, actual);
    }

    @Test
    public void testSetNombreNegativo(){
        test = new Puntuacion();
        test.setNombre("Laguna");
        String actual = "Laguna";
        String esperado = test.getNombre();
        assertNotEquals("QWERTY", actual);
    }

    @Test
    public void testSetPuntuacionNegativo(){
        test = new Puntuacion();
        test.setPuntuacion(100);
        int actual = 100;
        int esperado = test.getPuntuacion();
        assertNotEquals(2 , actual);
    }

    @Test
    public void testSetPuntuacionNegativo2(){
        test = new Puntuacion();
        test.setPuntuacion(100);
        int actual = 100;
        int esperado = test.getPuntuacion();
        assertNotEquals(esperado , actual);
    }

    @Test
    public void testSetNombreNegativo2(){
        test = new Puntuacion();
        test.setNombre("Laguna");
        String actual = "Laguna";
        String esperado = test.getNombre();
        assertNotEquals(esperado, actual);
    }
}