package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Teleport extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Teleport(float x, float y, Stage s) {
		super(x, y, s);
	
		loadAnimationFromSheet("teleport.png", 8, 8,  0.08f, false);
		//setSpeed(400);
		setSize(150, 150);
		 addAction( Actions.fadeOut(0.5f) );
	     addAction( Actions.after( Actions.removeActor() ) );
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
		super.act(dt);
		applyPhysics(dt);
	} 

}
