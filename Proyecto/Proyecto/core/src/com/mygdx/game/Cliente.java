package com.mygdx.game;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.mlaguna.Codec;

public class Cliente /*extends JFrame*/ {
	/*Container contenedor;
	JLabel titulo;
	JLabel nombre;
	JLabel puntuacion;
	JPanel panel;*/
	
	private String host;
	private int puerto;
	
	private Codec codec;
	private String mensajeCifrado;
	private String mensajeDescifrado;
	private String mensajeCifrado2;
	private String listadoDescifrado;
	
	private String nombre;
	private int puntuacion;
	Properties conf;
	
	/*public Cliente(String host, int puerto) {
		init();
		pack();
		setSize(450, 200);
		setLocationRelativeTo(null);
		setVisible(true);
	}*/
	
	/**
	 * Constructor de la clase Cliente
	 * @param host = recibe un String con el nombre del host
	 * @param puerto = recibe un int con el puerto de conexion
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 */
	public Cliente(String host, int puerto) throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.host = host;
		this.puerto = puerto;
		codec = new Codec();
		conf = new Properties();
		try {
			conf.load(new FileInputStream("configuracion.conf"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		puntuacion = Integer.valueOf(conf.getProperty("puntuacion"));
	}
	
	/**
	 * Metodo que permite la conexion y comunicacion entre los Sockets
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidAlgorithmParameterException
	 */
	public void conectar() throws UnknownHostException, IOException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
		Socket socketCliente = new Socket(host, puerto);
        DataInputStream entrada = new DataInputStream(socketCliente.getInputStream());
        PrintStream salida = new PrintStream(socketCliente.getOutputStream());
        String recibido;
        String mensaje;
        
        nombre = "Mario";
       
        
        mensajeCifrado = codec.cifrar(nombre);
        salida.println(mensajeCifrado);
        System.out.println("Mensaje enviado cifrado: " + mensajeCifrado);
        
        recibido = entrada.readLine();
        System.out.println("Mensaje del servidor cifrado: " + recibido);
        mensajeDescifrado = codec.descrifrar(recibido);
        System.out.println("Mensaje descifrado: " + mensajeDescifrado);
      
        mensajeCifrado2 = codec.cifrar(String.valueOf(puntuacion));
        salida.println(mensajeCifrado2);
        System.out.println("Mensaje enviado cifrado: " + mensajeCifrado2);
        
        recibido = entrada.readLine();
        System.out.println("Listado de puntuaciones cifrado: " + recibido);
        listadoDescifrado = codec.descrifrar(recibido);
        System.out.println("Listado descifrado: " + listadoDescifrado);
        
        
        
        
      
        
        
	}

	/*public void init() {
		contenedor = new Container();
		contenedor = getContentPane();
		
		panel();
		
	}*/

	/*public void panel() {
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		titulo = new JLabel("TABLA DE PUNTUACIONES");
		nombre = new JLabel(nombreServidor);
		puntuacion = new JLabel(puntuacionServidor);
		panel.add(titulo, BorderLayout.NORTH);
		panel.add(nombre, BorderLayout.WEST);
		panel.add(puntuacion, BorderLayout.EAST);
		contenedor.add(panel);
		
	}*/
	
	/*public static void main(String[] args) throws UnknownHostException, IOException {
		Cliente cliente = new Cliente("localhost", 4444);
		cliente.conectar();
	}*/

}
