package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GoblinMuerte extends BaseActor{

	String[] imagenesMuerte = {
			"DeathEnemy101.png",
			"DeathEnemy102.png",
			"DeathEnemy103.png",
			"DeathEnemy104.png",
	};
	
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public GoblinMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesMuerte, 0.08f, false);
		setScale(2);
		
	}
	
	

}
