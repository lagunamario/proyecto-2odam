package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Muerte extends BaseActor{

	String[] muerte = {
			"Death01.png",
			"Death02.png",
			"Death03.png",
			"Death04.png",
			"Death05.png",
			"Death06.png",
			"Death07.png"
	};
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Muerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(muerte, 0.08f, false);
		//setScale(2);
	}
	
	

}
