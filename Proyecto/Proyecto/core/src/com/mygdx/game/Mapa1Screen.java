package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;


public class Mapa1Screen extends BaseScreen {
	Personaje personaje;
	Goblin goblin;
	BaseGame baseGame;
	Seta seta;
	
	Label vidaLabel;
	Label mensajeLabel;
	Label puntuacionLabel;
	private BaseActor vidaImagen1;
	private BaseActor vidaImagen2;
	private BaseActor vidaImagen3;
	
	private int vidaGoblin = goblin.getVida();
	private int vidaSeta = seta.getVida();

	boolean gameOver = false;
	private long ultimo;
	
	int vida;
	int puntuacion;
	int powerUp;
	int iframes = 0;
	private long iframesTiempo;
	
	Properties conf;
	private Sound musicaFondo;
	private Sound musicaFondoOver;
	private float audioVolume;
	

	/**
	 * Metodo que ejecutara instrucciones al iniciar dicha clase
	 * Carga el mapa hecho en tilemap
	 * Carga la musica
	 * Carga los objetos hechos en tilemap
	 * Lee el archivo de configuracion
	 */
	@Override
	public void initialize() {
		
		conf = new Properties();
		try {
			conf.load(new FileInputStream("configuracion.conf"));
			puntuacion = Integer.valueOf(conf.getProperty("puntuacion"));
			vida = Integer.valueOf(conf.getProperty("vida"));
			powerUp = Integer.valueOf(conf.getProperty("powerup"));
			//iframes = Integer.valueOf(conf.getProperty("iframes"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		TilemapActor tma = new TilemapActor("mapa2.tmx", mainStage);
		System.out.println("Tu vida es: " + vida + " " + puntuacion);
		musicaFondo = Gdx.audio.newSound(Gdx.files.internal("nivel1y2.mp3"));
		audioVolume = 0.20f;
		musicaFondo.play(audioVolume);
		for (MapObject obj : tma.getRectangleList("Solid") ){
            MapProperties props = obj.getProperties();
            new Solid( (float)props.get("x"), (float)props.get("y"),
                (float)props.get("width"), (float)props.get("height"), 
                mainStage );
            System.out.println("Hola");
        }
		
		
		
		MapObject startPoint = tma.getRectangleList("start").get(0);
        MapProperties startProps = startPoint.getProperties();
        personaje = new Personaje( (float)startProps.get("x"), (float)startProps.get("y"), mainStage);
        
        for (MapObject obj : tma.getTileList("next") ){
            MapProperties props = obj.getProperties();
            new SiguienteNivel( (float)props.get("x"), (float)props.get("y"), mainStage );
        }
        
        
        /*for (MapObject obj : tma.getTileList("Suelo") )
        {
            MapProperties props = obj.getProperties();
            new Suelo( (float)props.get("x"), (float)props.get("y"), mainStage );
        }*/
        
        for (MapObject obj : tma.getTileList("Goblin")) {
        	MapProperties props = obj.getProperties();
        	goblin = new Goblin( (float)props.get("x"), (float)props.get("y"), mainStage);
        }
        
        for (MapObject obj : tma.getTileList("Seta")) {
        	MapProperties props = obj.getProperties();
        	seta = new Seta( (float)props.get("x"), (float)props.get("y"), mainStage);
        }
        
        /*for (MapObject obj : tma.getTileList("Esqueleto")) {
        	MapProperties props = obj.getProperties();
        	new Esqueleto( (float)props.get("x"), (float)props.get("y"), mainStage);
        }*/
        
        	//vida = personaje.getVida();
        	//vidaLabel = new Label("x " + vida, BaseGame.labelStyle);
        	//vidaLabel.setColor(Color.PINK);
        	//mensajeLabel = new Label("...", BaseGame.labelStyle);
        	//mensajeLabel.setVisible(false);
        	
      
        	//vidaContador = 3;
  
        
	        
        	vidaImagen1 = new BaseActor(0, 0, uiStage);
        	vidaImagen1.loadTexture("heart-icon.png");
        	vidaImagen2 = new BaseActor(0, 0, uiStage);
        	vidaImagen2.loadTexture("heart-icon.png");
        	vidaImagen3 = new BaseActor(0, 0, uiStage);
        	vidaImagen3.loadTexture("heart-icon.png");
        	
        	puntuacionLabel = new Label(null, BaseGame.labelStyle);
        	
        	uiTable.pad(20);
        	uiTable.add(vidaImagen1);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen2);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen3);
        	//uiTable.add().expandX();
        	uiTable.row();
        	uiTable.add(mensajeLabel).colspan(8).expandX().expandY();
        	uiTable.add(puntuacionLabel).colspan(8).expand(10, 10);
        	//uiTable.row();
        	
        	
        
        	personaje.toFront();
    	}

	/**
	 * Este metodo permite ejecutar diferentes instrucciones que afectan al juego en tiempo real
	 * Aqui se gestionara la vida del personaje principal, modificar constantemente el archivo de configuracion,
	 * y los diferentes comportamientos entre los elementos del juego
	 */
	@Override
	public void update(float dt) {
		
		conf.setProperty("vida", String.valueOf(vida));
	 	conf.setProperty("puntuacion", String.valueOf(puntuacion));
	 	conf.setProperty("powerup", String.valueOf(powerUp));
	 	//conf.setProperty("iframes", String.valueOf(iframes));
	 	try {
			conf.store(new FileOutputStream("configuracion.conf"), 
			        "Fichero de parametros");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	
	 	//System.out.println(vida + " " + puntuacion);
		/*if (vida <= 100 || >= 51) {
			
		}
				
		if (vida <= 50 && vida >= 26) {
			vidaImagen1.remove();
			vidaContador--;
			
		}
		
		if (vida <= 25 && vida >= 1) {
			vidaImagen2.remove();
			vidaContador--;
		/
		
		if (vida <= 0) {
			vidaImagen3.remove();
			vidaContador--;
		}*/
	 	
	 	if (vida == 2) {
			vidaImagen1.remove();
		}
	 	
		if (vida == 1) {
			vidaImagen2.remove();
			vidaImagen1.remove();
		}
		
		if (vida == 0) {
			vidaImagen3.remove();
			vidaImagen2.remove();
			vidaImagen1.remove();
			gameOver = true;
		}
		
	
		 if (iframes == 1) {
			long tiempoActual = System.currentTimeMillis();
			if (iframesTiempo - tiempoActual < -2000) {
				iframesTiempo = tiempoActual;
				iframes--;
			}

		 }
		
		
		
		//vidaReal.setVida(vida);
		
		for (BaseActor personaje : BaseActor.getList(mainStage, Personaje.class.getName())) {
			if (gameOver) {
				musicaFondo.dispose();
				musicaFondoOver = Gdx.audio.newSound(Gdx.files.internal("over.mp3"));
				audioVolume = 0.20f;
				musicaFondoOver.play(audioVolume);
				System.out.println("Has muerto");
				personaje.remove();
				Muerte muerte = new Muerte(0, 0, mainStage);
				muerte.centerAtActor(personaje);
				muerte.setY(100);
				BaseActor over = new BaseActor(0, 0, mainStage);
				over.loadTexture("over.png");
				over.setX(personaje.getX() - 150);
				over.setY(personaje.getY() + 200);
		        over.moveBy(0, 100);
		        puntuacionLabel.setText("Puntuacion: " + puntuacion);
		        puntuacionLabel.setColor(Color.WHITE);
		        puntuacionLabel.setFontScale(0.40f);
		        puntuacionLabel.setVisible(true);
		       
		        Cliente cliente = null;
				try {
					cliente = new Cliente("localhost", 4444);
				} catch (NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchPaddingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					try {
						cliente.conectar();
					} catch (InvalidKeyException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchPaddingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (BadPaddingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalBlockSizeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvalidAlgorithmParameterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        gameOver = false;
			}
		}
			
		for(BaseActor siguienteNivel : BaseActor.getList(mainStage, SiguienteNivel.class.getName()) ) {
	        if (personaje.overlaps(siguienteNivel) ) {
	        	System.out.println("colisiona");
	        	musicaFondo.dispose();
	        	baseGame.setActiveScreen(new Mapa1ScreenBoss());
	        		
	        }
	    }
		
		 for (BaseActor actor : BaseActor.getList(mainStage, Solid.class.getName())){
			 Solid solid = (Solid)actor;
			 
			 if (personaje.overlaps(solid) && solid.isEnabled()){
				 //System.out.println("ha");
				 Vector2 offset = personaje.preventOverlap(solid);
	                if (offset != null){
	                    if ( Math.abs(offset.x) > Math.abs(offset.y) ) {
	                        personaje.velocityVec.x = 0;
	                    }
	                    else {
	                        personaje.velocityVec.y = 0;
	                    }
	                }
	         }
			 /*if (goblin.overlaps(solid) && solid.isEnabled()){
				 //System.out.println("ha");
				 Vector2 offset = goblin.preventOverlap(solid);
	                if (offset != null){
	                    if ( Math.abs(offset.x) > Math.abs(offset.y) ) {
	                        goblin.velocityVec.x = 0;
	                    }
	                    else {
	                        goblin.velocityVec.y = 0;
	                    }
	                }
	            }*/
	        }
		 
		 for (BaseActor goblin : BaseActor.getList(mainStage, Goblin.class.getName())) {
			 if (iframes == 0 ) {
				 if (personaje.overlaps(goblin)) {
					personaje.preventOverlap(goblin);
				 	vida--;
				 	puntuacion--;
				 	iframes++;
				 }
			 }
			 else {
				 if (personaje.overlaps(goblin)) {
					personaje.preventOverlap(goblin);
				 }
			 }
			 
			 if (goblin.getY() < 100 || goblin.getY() > 100) {
				 goblin.setY(110);
			 }
			 
			 /*for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())){
				 Goblin goblin2 = (Goblin) goblin;
				 if (goblin2.belowSensor.overlaps(solid)) {
					 goblin2.gravedadActivate = false;
					 if(goblin2.belowSensor.getY() < solid.getY() + solid.getHeight()- 5) {
						 //goblin2.setY(goblin2.getY()+10);
					 }
					 goblin2.preventOverlap(solid);
				 }
			 }*/
			 
			 boolean distancia = personaje.isWithinDistance(350, goblin);
			 Goblin goblin2 = (Goblin) goblin;
			 //goblin2.setAnimation(goblin2.stand);
	            if(distancia) {
	            	String[] imagenesRun = {
	            			"RunEnemy101.png",
	            			"RunEnemy102.png",
	            			"RunEnemy103.png",
	            			"RunEnemy104.png",
	            			"RunEnemy105.png",
	            			"RunEnemy106.png",
	            			"RunEnemy107.png"
	            	};
	            	goblin2.setAnimation(goblin2.loadAnimationFromFiles(imagenesRun, 0.1f, true));
	                //goblin.setSpeed(200);
	                /*Vector2 playerPosition = new Vector2( personaje.getX(), personaje.getY() );
	                Vector2 enemyPosition = new Vector2( goblin.getX(), goblin.getY() );
	                Vector2 hitVector = enemyPosition.sub( playerPosition  );*/
	                //goblin.setMotionAngle(hitVector.angle() - 180);
	                
	                if (personaje.getX() > goblin2.getX()) {
	                	goblin2.derecha = true;               	
	                }
	                else {
	                	goblin2.derecha = false;
	                }
	                goblin2.walking = true;
	                
	                
	            }
	            else {
	            	
	            	//goblin2.walking = false;
	            }
		 }
		 
		 for (BaseActor seta : BaseActor.getList(mainStage, Seta.class.getName())) {
			 if (personaje.overlaps(seta)) {
				 Veneno veneno = new Veneno(0, 0, mainStage);
				 //vidaContador--;
				 vida--;
				 puntuacion--;
				 System.out.println(vida + " " + puntuacion);
				 veneno.centerAtActor(seta);
				 seta.remove();
				 vidaSeta = 3;
				 //personaje.preventOverlap(seta);
				 SetaMuerte muerte = new SetaMuerte(0, 0, mainStage);
				 muerte.centerAtActor(seta);
				 muerte.setY(100);
		         
				 
			 }
			 
			 if (seta.getY() < 100) {
				 seta.setY(110);
			 }
			 
			 /*for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())){
				 Goblin goblin2 = (Goblin) goblin;
				 if (goblin2.belowSensor.overlaps(solid)) {
					 goblin2.gravedadActivate = false;
					 if(goblin2.belowSensor.getY() < solid.getY() + solid.getHeight()- 5) {
						 //goblin2.setY(goblin2.getY()+10);
					 }
					 goblin2.preventOverlap(solid);
				 }
			 }*/
			 
			 boolean distancia = personaje.isWithinDistance(300, seta);
			 Seta seta2 = (Seta) seta;
			 //seta2.setAnimation(seta2.stand);
	            if(distancia) {
	            	String[] imagenesRun = {
	            			"RunEnemy201.png",
	            			"RunEnemy202.png",
	            			"RunEnemy203.png",
	            			"RunEnemy204.png",
	            			"RunEnemy205.png",
	            			"RunEnemy206.png",
	            			"RunEnemy207.png",
	            			"RunEnemy208.png"
	            	};
	            	seta2.setAnimation(seta2.loadAnimationFromFiles(imagenesRun, 0.1f, true));
	                //goblin.setSpeed(200);
	                /*Vector2 playerPosition = new Vector2( personaje.getX(), personaje.getY() );
	                Vector2 enemyPosition = new Vector2( goblin.getX(), goblin.getY() );
	                Vector2 hitVector = enemyPosition.sub( playerPosition  );*/
	                //goblin.setMotionAngle(hitVector.angle() - 180);
	                
	                if (personaje.getX() > seta2.getX()) {
	                	seta2.derecha = true;               	
	                }
	                else {
	                	seta2.derecha = false;
	                }
	                seta2.walking = true;
	                
	               
	            }
	            else {
	            	
	            	//goblin2.walking = false;
	            }
		 }
		 
		 for (BaseActor disparo : BaseActor.getList(mainStage, ProyectilPersonaje.class.getName())) {
			 
			 for (BaseActor goblin : BaseActor.getList(mainStage, Goblin.class.getName())) {
				 if (disparo.overlaps(goblin)) {
					 	this.goblin.setVida(vidaGoblin -= 1);
					 	System.out.println(vidaGoblin);
					 	disparo.remove();
					 	if (vidaGoblin == 0) {
						 	goblin.remove();
						 	vidaGoblin = 3;
						 	puntuacion++;
						 	disparo.remove();
						 	GoblinMuerte muerte = new GoblinMuerte(0, 0, mainStage);
						 	muerte.centerAtActor(goblin);
						 	muerte.setY(100);	
						}
				 }
				 
			 }
			 
			 for (BaseActor seta : BaseActor.getList(mainStage, Seta.class.getName())) {
				 if (disparo.overlaps(seta)) {
					 	this.seta.setVida(vidaSeta -= 1);
					 	System.out.println(vidaSeta);
					 	disparo.remove();
					 	if (vidaSeta == 0) {
						 	seta.remove();
						 	vidaSeta = 3;
						 	puntuacion++;
						 	disparo.remove();
						 	SetaMuerte muerte = new SetaMuerte(0, 0, mainStage);
						 	muerte.centerAtActor(seta);
						 	muerte.setY(100);	
						}
				 }
			 }
			 
			 for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())){
	                if (disparo.overlaps(solid))
	                {
	                    disparo.preventOverlap(solid);
	                    disparo.setSpeed(0);
	                    disparo.addAction( Actions.fadeOut(0.5f) );
	                    disparo.addAction( Actions.after( Actions.removeActor()));
	                }

	            }
		 }
		 
		 for (BaseActor disparo : BaseActor.getList(mainStage, PowerUp.class.getName())) {
			 
			 /*for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())) {
				 if (disparo.overlaps(solid)) {
					 disparo.remove();
				 }
			 }*/
			 
			 for (BaseActor goblin : BaseActor.getList(mainStage, Goblin.class.getName())) {
				 //Goblin goblin2 = (Goblin) goblin;
				 if (disparo.overlaps(goblin)) {
						 goblin.remove(); 
						 puntuacion++;
						 disparo.remove();
						 GoblinMuerte muerte = new GoblinMuerte(0, 0, mainStage);
						 muerte.centerAtActor(goblin);
						 muerte.setY(100);		
						
				}
			 }
			 
			 for (BaseActor seta : BaseActor.getList(mainStage, Seta.class.getName())) {
				 if (disparo.overlaps(seta)) {
					 seta.remove(); 
					 puntuacion++;
					 disparo.remove();
					 SetaMuerte muerte = new SetaMuerte(0, 0, mainStage);
					 muerte.centerAtActor(seta);
					 muerte.setY(110);
					 
				 }
			 }
			 
			 for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())){
	                if (disparo.overlaps(solid))
	                {
	                    disparo.preventOverlap(solid);
	                    disparo.setSpeed(0);
	                    disparo.addAction( Actions.fadeOut(0.5f) );
	                    disparo.addAction( Actions.after( Actions.removeActor()));
	                }

	            }
		 }
		 
		 
		 
	}
	
	
}
