package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

public class PersonajeTEST extends BaseActor
{
	 	private Animation stand;
	    private Animation walk;

	    private float walkAcceleration;
	    private float walkDeceleration;
	    private float maxHorizontalSpeed;
	    private float gravity;
	    private float maxVerticalSpeed;

	    private Animation jump;
	    private float jumpSpeed;
	    
	    private BaseActor belowSensor;
    

    public PersonajeTEST(float x, float y, Stage s){
        super(x,y,s); 

        stand = loadAnimationFromSheet("Idle.png", 1, 6, 0.08f, true);
        walk = loadAnimationFromSheet("Run.png", 1, 8, 0.08f, true);
        
        maxHorizontalSpeed = 100;
        walkAcceleration   = 200;
        walkDeceleration   = 200;
        gravity            = 700;
        maxVerticalSpeed   = 1000;

       
        setBoundaryPolygon(8);

        jump = loadAnimationFromSheet("Jump.png", 1, 2, 0.08f, true);
        jumpSpeed = 450;

    }

    public void act(float dt)
    {
        super.act(dt);
        
        
       
        
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            accelerationVec.add( -walkAcceleration, 0 );

        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            accelerationVec.add( walkAcceleration, 0 );


        if ( !Gdx.input.isKeyPressed(Keys.RIGHT) && !Gdx.input.isKeyPressed(Keys.LEFT)  ){
            float decelerationAmount = walkDeceleration * dt;

            float walkDirection;

            if ( velocityVec.x > 0 ) {
                walkDirection = 1;
            }
            else {
                walkDirection = -1;
            }

            float walkSpeed = Math.abs( velocityVec.x );

            walkSpeed -= decelerationAmount;

            if (walkSpeed < 0)
                walkSpeed = 0;

            velocityVec.x = walkSpeed * walkDirection;
        }

   
        accelerationVec.add(0, -gravity);

        velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );
        
        

        velocityVec.x = MathUtils.clamp( velocityVec.x, 
            -maxHorizontalSpeed, maxHorizontalSpeed );

        moveBy( velocityVec.x * dt, velocityVec.y * dt );

        accelerationVec.set(0,0);
        
        belowSensor.setPosition( getX() + 4, getY() - 8 );

        alignCamera();
        boundToWorld();
       
        
    }
    
    public boolean belowOverlaps(BaseActor actor){
        return belowSensor.overlaps(actor);
    }
    
    public boolean isOnSolid(){
        for (BaseActor actor : BaseActor.getList( getStage(), Solid.class.getName() )){
            Solid solid = (Solid)actor;
            if ( belowOverlaps(solid) && solid.isEnabled() )
                return true;
        }   

        return false;
    }

}