package com.mlaguna;

import org.bson.types.ObjectId;

public class Puntuacion {
    private ObjectId id;
    private String nombre;
    private int puntuacion;

    public Puntuacion() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Override
    public String toString() {
        return
                "Nombre: " + nombre + '\'' +
                ", Puntuacion:" + puntuacion;
    }
}
