package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Disparo extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Disparo(float x, float y, Stage s) {
		super(x, y, s);
		String[] imagenesDisparo = {
				"DisparoPersonaje01.png",
				"DisparoPersonaje02.png",
				"DisparoPersonaje03.png",
				"DisparoPersonaje04.png",
				"DisparoPersonaje05.png",
				"DisparoPersonaje06.png",
				"DisparoPersonaje07.png",
				"DisparoPersonaje08.png",
				"DisparoPersonaje09.png",
				"DisparoPersonaje10.png",
				"DisparoPersonaje11.png",
				"DisparoPersonaje12.png",
		};
		
		loadAnimationFromFiles(imagenesDisparo, 0.1f, true);
		setSpeed(300);
		
		
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
		super.act(dt);
		applyPhysics(dt);
	}

}
