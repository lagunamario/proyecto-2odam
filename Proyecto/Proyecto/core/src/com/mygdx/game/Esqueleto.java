package com.mygdx.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Esqueleto extends BaseActor {
	float facingAngle;
	String[] imagenesIdle = {
			"IdleBoss101.png",
			"IdleBoss102.png",
			"IdleBoss103.png",
			"IdleBoss104.png"
	};
	
	String[] imagenesRun = {
			"RunBoss101.png",
			"RunBoss102.png",
			"RunBoss103.png",
			"RunBoss104.png"
	};
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Esqueleto(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesIdle, 0.1f, true);
		setSize(150, 150);
		setBoundaryPolygon(6);
		setSpeed(MathUtils.random(50, 80));
		setMotionAngle(MathUtils.random(0, 360));
		
		
	}
	

}
