package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class BonesMuerte extends BaseActor{

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public BonesMuerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromSheet("Skeleton Dead.png", 1, 15, 0.08f, false);
		setScale(2);
		
	}
	
	

}
