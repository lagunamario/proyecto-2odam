package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Boss1Muerte extends BaseActor{

	String[] imagenesMuerte = {
			"DeathBoss101.png",
			"DeathBoss102.png",
			"DeathBoss103.png",
			"DeathBoss104.png",
	};
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Boss1Muerte(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromFiles(imagenesMuerte, 0.08f, false);
		//setScale(2);
		
	}
	
	

}
