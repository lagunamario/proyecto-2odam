package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;


public class Personaje extends BaseActor {
	private Animation stand;
    private Animation walk;
    //public Animation disparo;

    private float walkAcceleration;
    private float walkDeceleration;
    private float maxHorizontalSpeed;
    private float gravity;
    private float maxVerticalSpeed;
    


    private Animation jump;
    private float jumpSpeed;
    private BaseActor belowSensor;
    
   private Sound shot;
   private Sound foot;
   private Sound dash;
    
    
    private String nombre;
    private String puntuacion;
    
    private long ultimoDisparo;
	private long ultimoSalto;
	private long ultimoDash;

	boolean derecha;	
	
	BaseGame baseGame;
	
	Properties conf;
	
	
	int vida;
	int puntuaciones;
	int powerUp;
	private int ultimoPaso;
	private int tiempoActual;
	private float audioVolume;
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Personaje(float x, float y, Stage s) {
		super(x, y, s);
		derecha = false;
		conf = new Properties();
		nombre = "XYZ";
		puntuacion = "1995";
		
		
		String[] imagenesIdle = {
				"Idle01.png",
				"Idle02.png", 
				"Idle03.png", 
				"Idle04.png", 
				"Idle05.png", 
				"Idle06.png"};
		String[] imagenesWalk = {
				"Walk01.png",
				"Walk02.png", 
				"Walk03.png", 
				"Walk04.png", 
				"Walk05.png", 
				"Walk06.png",
				"Walk07.png",
				"Walk08.png"};
		String[] imagenesJump = {
				"Jump01.png",
				"Jump02.png"};
		
		String[] imagenesDisparo = {
				"AttackPersonaje01.png",
				"AttackPersonaje02.png",
				"AttackPersonaje03.png",
				"AttackPersonaje04.png",
				"AttackPersonaje05.png",
				"AttackPersonaje06.png",
				"AttackPersonaje07.png",
				"AttackPersonaje08.png",
		};
		
		conf = new Properties();
		try {
			conf.load(new FileInputStream("configuracion.conf"));
			powerUp = Integer.valueOf(conf.getProperty("powerup"));
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		stand = loadAnimationFromFiles(imagenesIdle, 0.08f, true);
		walk = loadAnimationFromFiles(imagenesWalk, 0.07f, true);
		//disparo = loadAnimationFromFiles(imagenesDisparo, 0.07f, false);
		//stand = loadAnimationFromSheet("Idle.png", 1, 6, 0.08f, true);
		//walk = loadAnimationFromSheet("Run.png", 1, 8, 0.08f, true);
		
		
		maxHorizontalSpeed = 200;
        walkAcceleration   = 800;
        walkDeceleration   = 800;
        gravity            = 700; //700
        maxVerticalSpeed   = 10000; //10000
       
        
        jump = loadAnimationFromFiles(imagenesJump, 0.08f, true);
        jumpSpeed = 450;
        
        belowSensor = new BaseActor(0,0, s);
        belowSensor.loadTexture("white.png");
        belowSensor.setSize( this.getWidth() - 8, 8 );
        belowSensor.setBoundaryRectangle();
        belowSensor.setVisible(false);
       
        shot = Gdx.audio.newSound(Gdx.files.internal("shot.ogg"));
        foot = Gdx.audio.newSound(Gdx.files.internal("foot.ogg"));
        dash = Gdx.audio.newSound(Gdx.files.internal("tp.ogg"));
        
        audioVolume = 0.20f;
        
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	 public void act(float dt){
	        super.act( dt );
	        
	        try {
				conf.load(new FileInputStream("configuracion.conf"));
				powerUp = Integer.valueOf(conf.getProperty("powerup"));
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   
	        //System.out.println("Posicion X: " + getX() + " Posicion Y: " + getY());
	        
	        
	        if (getY() < 100) {
	        	setY(100); 
	        }
	        
	        if (Gdx.input.isKeyPressed(Keys.A)) {
	            accelerationVec.add( -walkAcceleration, 0 );
	            if (ultimoPaso - tiempoActual < -500) {
                	ultimoPaso = tiempoActual;
                	foot.play(audioVolume);
                }
	            
	        }

	        if (Gdx.input.isKeyPressed(Keys.D)) {
	            accelerationVec.add( walkAcceleration, 0 );
	            if (ultimoPaso - tiempoActual < -500) {
                	ultimoPaso = tiempoActual;
                	foot.play(audioVolume);
                }
	        }

	        // decelerate when not accelerating
	        if ( !Gdx.input.isKeyPressed(Keys.D)
	        && !Gdx.input.isKeyPressed(Keys.A)  ){
	            float decelerationAmount = walkDeceleration * dt;

	            float walkDirection;

	            if ( velocityVec.x > 0 )
	                walkDirection = 1;
	            else
	                walkDirection = -1;

	            float walkSpeed = Math.abs( velocityVec.x );

	            walkSpeed -= decelerationAmount;

	            if (walkSpeed < 0)
	                walkSpeed = 0;

	            velocityVec.x = walkSpeed * walkDirection;
	        }
	        
	        if (Gdx.input.isKeyPressed(Keys.W)) {
				saltoPersonaje();
	        }
	        
	        if (Gdx.input.isKeyPressed(Keys.SPACE)) {
	        	disparoPersonaje();
	        }
	        
	        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) {
				dashPersonaje();
	        }
	        

	        // apply gravity
	        if (!this.isOnSolid()) {
	        	accelerationVec.add(0, -gravity);
	        }

	        velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );

	        velocityVec.x = MathUtils.clamp( velocityVec.x, 
	            -maxHorizontalSpeed, maxHorizontalSpeed );

	        moveBy( velocityVec.x * dt, velocityVec.y * dt );

	        // reset acceleration
	        accelerationVec.set(0,0);

	        // move the below sensor below the koala
	        belowSensor.setPosition( getX() + 4, getY() - 5 );

	        // manage animations
	        if ( this.isOnSolid()){
	            belowSensor.setColor( Color.GREEN );
	            if ( velocityVec.x == 0 ) {
	            	setAnimation(stand);
	            }  
	            else {
	            	setAnimation(walk);
	            }
	        }
	        else
	        {
	            belowSensor.setColor( Color.RED );
	            setAnimation(jump);
	        }

	        if ( velocityVec.x > 0 ) { // derecha
	            setScaleX(1);
	            derecha = true;
	            //System.out.println("Miras a la derecha " + derecha);
	        }

	        if ( velocityVec.x < 0 ) { // izquierda
	            setScaleX(-1);
	            derecha = false;
	            //System.out.println("Miras a la izquierda " + derecha);
	        }
	      

	        alignCamera();
	        boundToWorld();
	        
	        
	        
	    }
	 
	 	/**
	 	 * Metodo que envia un boolean para comprobar si el sensor de un BaseActor esta por debajo
	 	 * de la caja de colisiones
	 	 * @param actor = recibe un BaseActor
	 	 * @return
	 	 */
	    public boolean belowOverlaps(BaseActor actor)
	    {
	        return belowSensor.overlaps(actor);
	    }

	    /**
	     * Metodo que envia un boolean para comprobar si un actor esta sobre una superficie solida
	     * @return
	     */
	    public boolean isOnSolid()
	    {
	        for (BaseActor actor : BaseActor.getList( getStage(), Solid.class.getName() ))
	        {
	            Solid solid = (Solid)actor;
	            if ( belowOverlaps(solid) && solid.isEnabled() )
	                return true;
	        }   

	        return false;
	    }

	    /**
	     * Aplica la velocidad del salto al vector de velocidad de Y
	     */
	    public void jump()
	    {
	        velocityVec.y = jumpSpeed;
	    }

	    /**
	     * Metodo boolean que envia un boolean para ver si el BaseActor esta cayendo o no
	     * comrpobando que el vector de velocidad de Y sea menor a 0
	     * @return
	     */
	    public boolean isFalling()
	    {
	        return (velocityVec.y < 0);
	    }

	    public void spring()
	    {
	        velocityVec.y = 1.5f * jumpSpeed;
	    }    

	    /**
	     * Metodo boolean que envia un boolean para ver si el BaseActor se encuentra saltando
	     * comprobando que el vector de velocidad de Y sea mayor a 0
	     * @return
	     */
	    public boolean isJumping()
	    {
	        return (velocityVec.y > 0); 
	    }

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getPuntuacion() {
			return puntuacion;
		}

		public void setPuntuacion(int puntuaciones) {
			this.puntuaciones = puntuaciones;
		}
		
		public int getVida() {
			return vida;
		}

		public void setVida(int vida) {
			this.vida = vida;
		}

		/**
		 * Metodo para la realizacion del dash del personaje
		 */
		private void dashPersonaje() {
			long tiempoActual2 = System.currentTimeMillis();
			if (ultimoDash - tiempoActual2 < -800) {
				ultimoDash = tiempoActual2;
				if(derecha) {
					setX(getX() + 150);
				}
				else {
					setX(getX() - 150);
				}
				Teleport tp = new Teleport(0, 0, getStage());
				dash.play();
				tp.centerAtActor(this);
			}
			
		}

		/**
		 * Metodo para el salto del personaje
		 */
		private void saltoPersonaje() {
			long tiempoActual = System.currentTimeMillis();
			if (ultimoSalto - tiempoActual < -800) {
				ultimoSalto = tiempoActual;	
				setY(305);
				Teleport tp = new Teleport(0, 0, getStage());
				dash.play();
				tp.centerAtActor(this);
			}
		}

		/**
		 * Metodo para el correcto funcionamiento del disparo del personaje y la modificacion
		 * de este por el PowerUp.
		 */
		private void disparoPersonaje() {
			long tiempoActual = System.currentTimeMillis();
			if (ultimoDisparo - tiempoActual < -500) {
				ultimoDisparo = tiempoActual;			
				//setAnimation(disparo);
				if (powerUp == 0) {
					ProyectilPersonaje disparo = new ProyectilPersonaje(0, 0, getStage(), this);
					shot.play();
					if (derecha) {
						disparo.setPosition(getX() + getWidth() + 5, getY() + 50);
						System.out.println("Miras a la derecha " + derecha);
						if (disparo.isAnimationFinished()){
							disparo.remove();
						}
					} 
					else {
						//disparo.setScaleX(-1);
						//disparo.velocityVec.x = -400;
						disparo.setPosition(getX() -  5, getY() + 50);
						System.out.println("Miras a la derecha " + derecha);
						if (disparo.isAnimationFinished()){
							disparo.remove();
						}
					}
				}
				else {
					PowerUp disparo = new PowerUp(0, 0, getStage(), this);
					shot.play();
					if (derecha) {
						disparo.setPosition(getX() + getWidth() + 5, getY() + 50);
						System.out.println("Miras a la derecha " + derecha);
						if (disparo.isAnimationFinished()){
							disparo.remove();
						}
					} 
					else {
						//disparo.setScaleX(-1);
						//disparo.velocityVec.x = -400;
						disparo.setPosition(getX() -  5, getY() + 50);
						System.out.println("Miras a la derecha " + derecha);
						if (disparo.isAnimationFinished()){
							disparo.remove();
						}
					}
				}
				
				
			}
				
			
		}
		
	    
}
