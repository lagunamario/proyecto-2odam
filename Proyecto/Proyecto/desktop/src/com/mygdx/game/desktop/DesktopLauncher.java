package com.mygdx.game.desktop;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Cliente;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Parametros;

public class DesktopLauncher {
	
	public static void main (String[] args) throws UnknownHostException, IOException{
		Game myGame = new MyGdxGame(); 
		LwjglApplication launcher = new LwjglApplication( myGame, "Proyecto", 800, 640 );
		
		
	}
	
}
