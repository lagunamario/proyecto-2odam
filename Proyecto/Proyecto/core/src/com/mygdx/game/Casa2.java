package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;


public class Casa2 extends BaseScreen {
	Personaje personaje;
	Npc1 npc;
	SiguienteNivel sig;
	
	Label vidaLabel;
	Label mensajeLabel;
	private BaseActor vidaImagen1;
	private BaseActor vidaImagen2;
	private BaseActor vidaImagen3;
	BaseGame baseGame;
	Parametros par;
	
	int vida;
	int puntuacion;
	int interior;
	
	Properties conf;
	//private Sound musicaFondo;
	//private float audioVolume;
	
	private Sound door;

	/**
	 * Metodo que ejecutara instrucciones al iniciar dicha clase
	 * Carga el mapa hecho en tilemap
	 * Carga la musica
	 * Carga los objetos hechos en tilemap
	 * Lee el archivo de configuracion
	 */
	@Override
	public void initialize() {
		TilemapActor tma = new TilemapActor("casa2.tmx", mainStage);
		
		System.out.println("Interior " +  interior);
		/*musicaFondo = Gdx.audio.newSound(Gdx.files.internal("aldea.mp3"));
		audioVolume = 0.20f;
		musicaFondo.play(audioVolume);*/
		for (MapObject obj : tma.getRectangleList("Solid") ){
            MapProperties props = obj.getProperties();
            new Solid( (float)props.get("x"), (float)props.get("y"),
                (float)props.get("width"), (float)props.get("height"), 
                mainStage );
            System.out.println("Hola");
        }
		
		conf = new Properties();
		try {
			conf.load(new FileInputStream("configuracion.conf"));
			
			vida = Integer.valueOf(conf.getProperty("vida"));
			puntuacion = Integer.valueOf(conf.getProperty("puntuacion"));
			interior = Integer.valueOf(conf.getProperty("interior"));
			System.out.println("Tu vida es: " + vida + " " + puntuacion);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MapObject startPoint = tma.getRectangleList("start").get(0);
        MapProperties startProps = startPoint.getProperties();
        personaje = new Personaje( (float)startProps.get("x"), (float)startProps.get("y"), mainStage);
        
        interior = 2;
        
        for (MapObject obj : tma.getTileList("next") ){
            MapProperties props = obj.getProperties();
           sig = new SiguienteNivel( (float)props.get("x"), (float)props.get("y"), mainStage );
        }
        
        
        /*for (MapObject obj : tma.getTileList("Suelo") )
        {
            MapProperties props = obj.getProperties();
            new Suelo( (float)props.get("x"), (float)props.get("y"), mainStage );
        }*/
        
        	
        	vidaLabel = new Label("x " + vida, BaseGame.labelStyle);
        	vidaLabel.setColor(Color.PINK);
        	mensajeLabel = new Label("...", BaseGame.labelStyle);
        	mensajeLabel.setVisible(false);
        	
        	/*vidaImagen1 = new BaseActor(0, 0, uiStage);
        	vidaImagen1.loadTexture("heart-icon.png");
        	vidaImagen2 = new BaseActor(0, 0, uiStage);
        	vidaImagen2.loadTexture("heart-icon.png");
        	vidaImagen3 = new BaseActor(0, 0, uiStage);
        	vidaImagen3.loadTexture("heart-icon.png");*/
        	
        	uiTable.pad(20);
        	uiTable.add(vidaImagen1);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen2);
        	//uiTable.add().expandX();
        	uiTable.row();
        	//uiTable.pad(20);
        	uiTable.add(vidaImagen3);
        	//uiTable.add().expandX();
        	uiTable.row();
        	uiTable.add(mensajeLabel).colspan(8).expandX().expandY();
        	//uiTable.row();
        	
        	
        
        	personaje.toFront();
        	
        	door = Gdx.audio.newSound(Gdx.files.internal("door.ogg"));
    	}

	/**
	 * Este metodo permite ejecutar diferentes instrucciones que afectan al juego en tiempo real
	 * Aqui se gestionara la vida del personaje principal, modificar constantemente el archivo de configuracion,
	 * y los diferentes comportamientos entre los elementos del juego
	 */
	@Override
	public void update(float dt) {
		
		conf.setProperty("vida", String.valueOf(vida));
	 	conf.setProperty("puntuacion", String.valueOf(puntuacion));
	 	conf.setProperty("interior", String.valueOf(interior));
	 	try {
			conf.store(new FileOutputStream("configuracion.conf"), 
			        "Fichero de parametros");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	
	 	/*if (vida == 2) {
			vidaImagen1.remove();
		}
	 	
		if (vida == 1) {
			vidaImagen2.remove();
			vidaImagen1.remove();
		}
		
		if (vida == 0) {
			vidaImagen3.remove();
			//gameOver = true;
		}*/
		
		
		 for (BaseActor actor : BaseActor.getList(mainStage, Solid.class.getName())){
			 Solid solid = (Solid)actor;
			 
			 
			 if (personaje.overlaps(solid) && solid.isEnabled()){
				 //System.out.println("ha");
				 Vector2 offset = personaje.preventOverlap(solid);
	                if (offset != null){
	                    if ( Math.abs(offset.x) > Math.abs(offset.y) ) {
	                        personaje.velocityVec.x = 0;
	                    }
	                    else {
	                        personaje.velocityVec.y = 0;
	                    }
	                }
	            }
	        }
		 
		
		 
		 for(BaseActor siguienteNivel : BaseActor.getList(mainStage, SiguienteNivel.class.getName()) ) {
		        if (personaje.overlaps(siguienteNivel) && Gdx.input.isKeyPressed(Keys.ENTER) ) {
		        	door.play();
		        	System.out.println("colisiona");
		        	baseGame.setActiveScreen(new AldeaScreen());
		        	
	
		        }
		 }
		 
		 for (BaseActor disparo : BaseActor.getList(mainStage, ProyectilPersonaje.class.getName())) {
			 
			 for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())){
	                if (disparo.overlaps(solid))
	                {
	                    disparo.preventOverlap(solid);
	                    disparo.setSpeed(0);
	                    disparo.addAction( Actions.fadeOut(0.5f) );
	                    disparo.addAction( Actions.after( Actions.removeActor() ) );
	                }

	         }
			 
			 for (BaseActor npc : BaseActor.getList(mainStage, Npc1.class.getName())){
	                if (disparo.overlaps(npc))
	                {
	                    disparo.preventOverlap(npc);
	                    disparo.setSpeed(0);
	                    disparo.addAction( Actions.fadeOut(0.5f) );
	                    disparo.addAction( Actions.after( Actions.removeActor() ) );
	                }

	         }
		 }
		 
	}
}
