package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class SiguienteNivelAldea1 extends BaseActor {

	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public SiguienteNivelAldea1(float x, float y, Stage s) {
		super(x, y, s);
		setSize(32, 32);
		setBoundaryRectangle();
		
		
	}

}
