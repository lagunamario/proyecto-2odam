package com.mlaguna;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, MouseListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        init();
    }

    private void init(){
        addActionListener(this);
        addListSelectionListeners(this);
        addMouseListener(this);
        modelo.conectar();
        listar(modelo.getPuntuaciones());
    }

    private void addMouseListener(MouseListener listener) {
        vista.listPuntuacion.addMouseListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listPuntuacion.addListSelectionListener(listener);
    }

    private void addActionListener(ActionListener listener) {
        vista.btnNuevaPuntuacion.addActionListener(listener);
        vista.btnModificarPuntuacion.addActionListener(listener);
        vista.btnEliminarPuntuacion.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Puntuacion p;

        switch(comando){
            case "nueva":
                p = new Puntuacion();
                setDatos(p);
                modelo.guardarPuntuacion(p);
                listar(modelo.getPuntuaciones());
                limpiarCampos();
                break;

            case "modificar":
                p = (Puntuacion) vista.listPuntuacion.getSelectedValue();
                setDatos(p);
                modelo.modificarPuntuacion(p);
                listar(modelo.getPuntuaciones());
                limpiarCampos();
                break;

            case "eliminar":
                p = (Puntuacion) vista.listPuntuacion.getSelectedValue();
                modelo.borrarPuntuacion(p);
                listar(modelo.getPuntuaciones());
                limpiarCampos();
                break;

        }
    }

    public void limpiarCampos(){
        vista.txtNombre.setText("");
        vista.txtPuntuacion.setText("");
    }

    private void mostrarCampos(){
        Puntuacion p = (Puntuacion) vista.listPuntuacion.getSelectedValue();
        vista.txtNombre.setText(p.getNombre());
        vista.txtPuntuacion.setText(String.valueOf(p.getPuntuacion()));
    }

    public void setDatos(Puntuacion p){
        p.setNombre(vista.txtNombre.getText());
        p.setPuntuacion(Integer.parseInt(vista.txtPuntuacion.getText()));
    }

    public void listar(List<Puntuacion> lista){
        vista.dlmPuntuacion.clear();
        for (Puntuacion p : lista) {
            vista.dlmPuntuacion.addElement(p);
        }
    }

    public void listarCliente(List<Puntuacion> lista){
        for (Puntuacion p : lista){
            System.out.println(p);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.listPuntuacion){
            mostrarCampos();
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
