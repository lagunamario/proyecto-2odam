package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Boss2 extends BaseActor {
	 private float walkAcceleration;
	 private float walkDeceleration;
	 private float maxHorizontalSpeed;
	 private float gravity;
	 private float maxVerticalSpeed;
	 private float jumpSpeed;
	 public BaseActor belowSensor;
	 private BaseActor colisionBox;
	 
	 public Animation stand;
	 private Animation walk;
	 private Animation jump;
	 private Animation muerte;
	 
	 public boolean walking;
	 public boolean derecha = false;
	 public boolean gravedadActivate = true;
	 
	 public boolean isDead;
	 
	 public int vida;
	 
	String[] imagenesIdle = {
			"Boss2Idle01.png",
			"Boss2Idle02.png",
			"Boss2Idle03.png",
			"Boss2Idle04.png",
			"Boss2Idle05.png",
			"Boss2Idle06.png",
			"Boss2Idle07.png",
			"Boss2Idle08.png",
			"Boss2Idle09.png",
			"Boss2Idle10.png",
			"Boss2Idle11.png",
			"Boss2Idle12.png",
			"Boss2Idle13.png",
			"Boss2Idle14.png",
			"Boss2Idle15.png",
			"Boss2Idle16.png",
			"Boss2Idle17.png",
			"Boss2Idle18.png",
	};
	
	String[] imagenesRun = {
			"RunBoss101.png",
			"RunBoss102.png",
			"RunBoss103.png",
			"RunBoss104.png"

	};
	
	/*String[] imagenesMuerte = {
			"DeathBoss101.png",
			"DeathBoss102.png",
			"DeathBoss103.png",
			"DeathBoss104.png",
	};*/
	
	/**
	 * Constructor de un BaseActor
	 * @param x = representa la posicion X
	 * @param y = representa la posicion Y
	 * @param s = representa por que Stage esta siendo influenciado
	 */
	public Boss2(float x, float y, Stage s) {
		super(x, y, s);
		walking = false;
		isDead = false;
		stand = loadAnimationFromFiles(imagenesIdle, 0.1f, true);
		//walk = loadAnimationFromFiles(imagenesRun, 0.1f, true);
		//jump = loadAnimationFromFiles(imagenesRun, 0.1f, true);
		//muerte = loadAnimationFromFiles(imagenesMuerte, 1f, false);�
		muerte = loadAnimationFromSheet("Boss2Death.png", 1, 30, 0.08f, false );
		vida = 10;
		
		setScale(2);
		//setBoundaryPolygon(6);
		//setSpeed(MathUtils.random(50, 80));
		//setMotionAngle(MathUtils.random(0, 360));
		
		maxHorizontalSpeed = 50;
        walkAcceleration   = 5;
        walkDeceleration   = 5;
        gravity            = 10;
        maxVerticalSpeed   = 25;
        
        colisionBox = new BaseActor(0, 0, s);
        colisionBox.loadTexture("colision.png");
        colisionBox.setVisible(false);
        colisionBox.setSize(68, 72);
        
        belowSensor = new BaseActor(0,0, s);
        belowSensor.loadTexture("white.png");
        belowSensor.setSize( this.getWidth() - 8, 8 );
        belowSensor.setBoundaryRectangle();
        belowSensor.setVisible(false);
		
		
	}
	
	/**
	 * Metodo que cumple la misma funcion que el update() en las BaseScreen pero para los BaseActor
	 * Actualiza las instrucciones a tiempo real
	 */
	public void act(float dt) {
        super.act(dt);
        
        colisionBox.centerAtActor(this);
        
        /*float angle = getMotionAngle();
        
        if (angle >= 45 && angle <= 135){
            facingAngle = 90;
            
        }
        else if (angle > 135 && angle < 225){
            facingAngle = 180;
            
        }
        else if (angle >= 225 && angle <= 315){
            facingAngle = 270;
            
        }
        else{
            facingAngle = 0;
               
        }*/
        
        /*if (walking) {
        	setAnimation(walk);
        	if (derecha) {
                accelerationVec.add(walkAcceleration, 0);
                if (velocityVec.x > maxHorizontalSpeed) {
                	velocityVec.x = maxHorizontalSpeed;
                }
            } else {
                accelerationVec.add(-walkAcceleration, 0);
                if (velocityVec.x < maxHorizontalSpeed) {
                	velocityVec.x = -maxHorizontalSpeed;
                }
            }
        
        }
        else {
        	setAnimation(stand);
        	accelerationVec.x = 0;
        	velocityVec.x = 0;
        }*/

        if (gravedadActivate) {
            //accelerationVec.add(0, -gravity);
        } else {
        	accelerationVec.y = 0;
            velocityVec.y = 0;
        }
        
        /*if (isDead) {
        	setAnimation(muerte);
        	accelerationVec.x = 0;
        	velocityVec.x = 0;
        }*/

        velocityVec.add(accelerationVec.x, accelerationVec.y);

        velocityVec.x = MathUtils.clamp(velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed);

        velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeed, maxVerticalSpeed);

        moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));
        
        belowSensor.setPosition( getX() + 4, getY() - 20 );
        
        if ( this.isOnSolid()){
        	
            belowSensor.setColor( Color.GREEN );
            if ( velocityVec.x == 0 ) {
            	setAnimation(stand);
            }  
            else {
            	//setAnimation(walk);
            }
        }
        else{
            belowSensor.setColor( Color.RED );
            //setAnimation(jump);
        }

        if ( velocityVec.x > 0 ) // face right
            setScaleX(2);

        if ( velocityVec.x < 0 ) // face left
            setScaleX(-2);
        
        applyPhysics(dt);
        boundToWorld();
        
        
        
	}
	/**
 	 * Metodo que envia un boolean para comprobar si el sensor de un BaseActor esta por debajo
 	 * de la caja de colisiones
 	 * @param actor = recibe un BaseActor
 	 * @return
 	 */
	public boolean belowOverlaps(BaseActor actor)
    {
        return belowSensor.overlaps(actor);
    }

	/**
     * Metodo que envia un boolean para comprobar si un actor esta sobre una superficie solida
     * @return
     */
    public boolean isOnSolid()
    {
        for (BaseActor actor : BaseActor.getList( getStage(), Solid.class.getName() ))
        {
            Solid solid = (Solid)actor;
            if ( belowOverlaps(solid) && solid.isEnabled() )
                return true;
        }   

        return false;
    }

    /**
     * Aplica la velocidad del salto al vector de velocidad de Y
     */
    public void jump()
    {
        velocityVec.y = jumpSpeed;
    }

    /**
     * Metodo boolean que envia un boolean para ver si el BaseActor esta cayendo o no
     * comrpobando que el vector de velocidad de Y sea menor a 0
     * @return
     */
    public boolean isFalling()
    {
        return (velocityVec.y < 0);
    }

    public void spring()
    {
        velocityVec.y = 1.5f * jumpSpeed;
    }    

    /**
     * Metodo boolean que envia un boolean para ver si el BaseActor se encuentra saltando
     * comprobando que el vector de velocidad de Y sea mayor a 0
     * @return
     */
    public boolean isJumping()
    {
        return (velocityVec.y > 0); 
    }
    
	

}
